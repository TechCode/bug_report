class ProcessBugsJob < ApplicationJob
  # include Sneakers::Worker
  queue_as :default

  def perform(number, data)
    unless b = Bug.create(data.merge(number: number))
      logger.info b.errors
    end
    logger.info b
  end
end
