module BugIndexer
  extend ActiveSupport::Concern

  included do
    include Elasticsearch::Model
    include Elasticsearch::Model::Callbacks

    settings index: { number_of_shards: 1} do
      mappings dynamic: 'false' do
        indexes :number, analyzer: 'english', index_options: 'offsets'
        indexes :status, analyzer: 'english', index_options: 'offsets'
        indexes :periority, analyzer: 'english', index_options: 'offsets'
        indexes :comment, analyzer: 'english', index_options: 'offsets'
      end
    end

    def as_indexed_json(options={})
      as_json(root: false,
              only: [:application_token, :status, :number, :priority, :comment, :state],
              include: {state: {only: [:os, :device, :memory, :storage]}})
    end

    def self.search(application_token, query)
      __elasticsearch__.search(
        {
          query: {
            bool: {
              # FIXME: appliaction_token filter not working!
              # filter: { term: { application_token: application_token } },
              must: {
                multi_match: {
                  query: query,
                  fields: ['status', 'number', 'priority', 'comment^10', 'state', 'os', 'device', 'memory', 'storage']
                }
              }
            }
          }
        }
      )
    end
  end
end
