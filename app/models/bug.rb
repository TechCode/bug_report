class Bug < ApplicationRecord
  include BugIndexer
  belongs_to :state
  accepts_nested_attributes_for :state
  validates :status, inclusion: { in: %w(new in-progress closed),
    message: "%{value} is not a valid status" }
  validates :priority, inclusion: { in: %w(minor major critical),
    message: "%{value} is not a valid priority" }

  def self.next_number(application_token)
    $redis = Redis.new(host: 'redis')
    $redis.incr "counter_#{application_token}"
  end
end
