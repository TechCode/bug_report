class Api::V1::BugsController < Api::V1::ApiBaseController
  def create
    number = Bug.next_number(request.headers['X-User-Token'])
    bugs_data = bug_params.to_hash
    ProcessBugsJob.perform_later(
      number,
      bugs_data
    )
    render json: {number: number}
  end

  def show
    @bug = Bug.where(
      number: params[:number],
      application_token: request.headers['X-User-Token']
    ).first
    # byebug
    respond_with(@bug)
  end

  private
  def bug_params
    params.require(:bug).permit(:status, :priority, :comment, state_attributes: [:device, :os, :memory, :storage]).merge(application_token: request.headers['X-User-Token'])
  end
end
