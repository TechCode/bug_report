class Api::V1::ApiBaseController < ApplicationController
  respond_to :json
  rescue_from ActiveRecord::RecordNotFound, ActiveRecord::InvalidForeignKey, :with => :not_found
  protect_from_forgery with: :null_session, except: [:rate],
                       if: Proc.new { |c| c.request.format == 'application/json' }

  def not_found
    render :json => {:error => 'object not found'}, :status => 404
  end
end
