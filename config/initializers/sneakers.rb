require 'sneakers'
require 'sneakers/metrics/logging_metrics'

opts = {
  amqp: 'amqp://rabbitmq',
  # vhost: 'username',
  # exchange: 'sneakers',
  # exchange_type: :direct,
  metrics: Sneakers::Metrics::LoggingMetrics.new
}

Sneakers.configure(opts)
