#!/bin/bash
set -e
cmd="$@"

# This entrypoint is used to play nicely with the current cookiecutter configuration.
# Since docker-compose relies heavily on environment variables itself for configuration, we'd have to define multiple
# environment variables just to support cookiecutter out of the box. That makes no sense, so this little entrypoint
# does all this for us.
export REDIS_URL=redis://redis:6379

# the official postgres image uses 'postgres' as default user if not set explictly.
if [ -z "$POSTGRES_USER" ]; then
    export POSTGRES_USER=postgres
fi

function postgres_ready(){
ruby << END
require 'pg'
begin
  conn = PG::Connection.open(dbname: "$POSTGRES_DB", dbuser: "$POSTGRES_USER", dbpassword: "$POSTGRES_PASSWORD")
  res = conn.exec('SELECT 1 AS a, 2 AS b, NULL AS c')
rescue
  exit 1
end
exit 0
END
}

# until postgres_ready; do
#   >&2 echo "Postgres is unavailable - sleeping"
#   sleep 1
# done

# >&2 echo "Postgres is up - continuing..."
exec $cmd
