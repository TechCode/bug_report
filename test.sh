#!/usr/bin/bash

# FIXME: needs to be more dyntamic!

curl -X POST \
  http://localhost:3000/api/v1/bugs \
  -H 'content-type: application/json' \
  -H 'x-user-token: moe-men' \
  -d '{"bug": {"status": "new", "priority": "minor", "comment": "yes it is", "state_attributes": {"device": "nexus 5", "os": "Android 6.0.1", "memory": "2", "storage": "16000"}}}'

curl -X GET \
  http://localhost:3000/api/v1/bugs/1 \
  -H 'content-type: application/json' \
  -H 'x-user-token: moe-men' \
